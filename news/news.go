package news

var (
	searchClause chan (string)

	returnClause chan (string)
	result       chan ([]Topic)
)

func init() {
	searchClause = make(chan (string))

	returnClause = make(chan (string))
	result = make(chan ([]Topic))
}

func Collect(category string) {
	searchClause <- category
}

func Result(category string) []Topic {
	returnClause <- category
	return <-result
}

func (a Archive) CollectNews() {
	for {
		select {
		case category := <-searchClause:
			a.collectNews(category)
		case category := <-returnClause:
			result <- a.data(category)
		}
	}
}
