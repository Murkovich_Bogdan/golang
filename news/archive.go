package news

type Topic struct {
	Title  string `json:"title"`
	Author string `json:"author"`
	URL    string `json:"url"`
}

type Archive map[string][]Topic

func NewArchive() Archive {
	return make(map[string][]Topic)
}

func (a Archive) collectNews(category string) {
	sources := getSources(category)
	topics := getTopics(sources)

	a[category] = topics
}

func (a Archive) data(category string) []Topic {
	return a[category]
}
