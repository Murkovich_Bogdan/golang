package main

import (
	"../golang/router"

	"../golang/news"
)

func main() {
	a := news.NewArchive()
	r := router.New()

	go a.CollectNews()

	r.Run()
}

// 51ab3dd578fe4748b79a4bfe78a34f68
